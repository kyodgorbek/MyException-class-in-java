# MyException-class-in-java



import javax.swing.JOptionPane;

public class myException {
 
 public static void main(String[] args){
   String numStr = JOption.showInputDialog("Number of Children");
   int children = 1;
   try {
   children = Integer.parseInt(numStr);
   double oneShare = 100.0 / children;
   System.out.println("Each child's share is " + " - default is ", "Error", JOptionPane.ERROR_MESSAGE);
  }
  catch (Exception ex) {
  // Handle all other exception types
  System.out.println(ex.getMessage());
  ex.printStackTrace();
 }
 finally {
   System.out.println("Number of children is " + children);
  }
 }
}   
     
